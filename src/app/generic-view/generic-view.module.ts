import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbModule } from 'xng-breadcrumb';

import { GenericViewComponent } from './generic-view.component';
// import { GenericViewRoutingModule } from './generic-view-routing.module';
import { SharedComponentsModule } from '../shared-components/shared-components.module';

const COMPONENTS = [GenericViewComponent];

@NgModule({
  declarations: COMPONENTS,
  imports: [
    CommonModule,
    // GenericViewRoutingModule,
    SharedComponentsModule,
    BreadcrumbModule
  ],
  exports: COMPONENTS
})
export class GenericViewModule {}
