import { Component, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-generic-view',
  templateUrl: './generic-view.component.html',
  styleUrls: ['./generic-view.component.less']
})
export class GenericViewComponent {
  @Input()
  public title: string = '';
}
