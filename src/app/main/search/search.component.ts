import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { AnalyzeApiService } from 'src/app/services/analyze-api.service';

@Component({
  selector: 'main-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.less']
})
export class SearchComponent implements OnInit {
  @ViewChild('searchInput') searchInput!: ElementRef<HTMLInputElement>;

  constructor(private analyzeApiServer: AnalyzeApiService) {}

  public ngOnInit(): void {
    this.handleInputSubmit();
  }

  public handleInputSubmit(): void {
    this.analyzeApiServer.classifyText(
      'en',
      'I really love scrambled eggs. They are the Shiiiiit.',
      'SHORT_TEXT'
    );
  }
}
