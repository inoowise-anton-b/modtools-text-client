import { Component, OnInit, OnDestroy } from '@angular/core';
import { AnalyzeApiService } from 'src/app/services/analyze-api.service';
import { Subscription } from 'rxjs';
import { Topics, Languages, Language } from 'src/constants';

@Component({
  selector: 'app-diagnose',
  templateUrl: './diagnose.component.html',
  styleUrls: ['./diagnose.component.less']
})
export class DiagnoseComponent implements OnInit, OnDestroy {
  public predictions!: Array<{ language: string; value: number }>;
  public textExtended!: Array<{ classified: string; original: string }>;
  public text!: string;
  public topics!: Array<{ name: string; icon: string; value: number }>;
  public languages: Language[] = Languages;
  public isOverlayOpened: boolean = false;

  private classifiedTextSubscriber: Subscription;

  constructor(private analyzeApiService: AnalyzeApiService) {}

  public ngOnInit(): void {
    this.classifiedTextSubscriber = this.analyzeApiService.classifiedText.subscribe(
      ({ predictions, extended, text, topics }) => {
        this.predictions = Object.keys(predictions).reduce(
          (previous, current) => {
            const newKey = current.split('_')[1] || '';
            return [
              ...previous,
              {
                language: newKey,
                value: Math.round(predictions[current] * 100)
              }
            ];
          },
          []
        );

        const extendedTextMap: { [text: string]: string } = extended.reduce(
          (previous, { original, solution }) => {
            const value = previous[solution] || '';
            return {
              ...previous,
              [solution]: [value, original].join(' ')
            };
          },
          {}
        );

        this.textExtended = Object.keys(extendedTextMap).map(key => ({
          classified: key,
          original: extendedTextMap[key]
        }));

        this.text = text;

        this.topics = Object.keys(topics).map(topic => ({
          ...Topics[topic],
          value: topics[topic]
        }));
      }
    );
  }

  public ngOnDestroy(): void {
    this.classifiedTextSubscriber.unsubscribe();
  }

  public toggleOverlayView(): void {
    this.isOverlayOpened = !this.isOverlayOpened;
  }
}
