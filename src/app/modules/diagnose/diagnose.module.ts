import { NgModule } from '@angular/core';
import { GenericViewModule } from 'src/app/generic-view/generic-view.module';
import { DiagnoseRoutingModule } from './diagnose-routing.module';
import { CommonModule } from '@angular/common';

import { DiagnoseComponent } from './diagnose.component';

const COMPONENTS = [DiagnoseComponent];

@NgModule({
  declarations: COMPONENTS,
  imports: [CommonModule, DiagnoseRoutingModule, GenericViewModule]
})
export class DiagnoseModule {}
