export interface ClassifyTextResponse {
  lingFragments: Array<{
    endPos: number;
    normalized: string;
    redactedText: string;
    startPos: number;
    text: string;
    topics: { [index: number]: number };
  }>;
  language: string;
  predictions: { lang_en: number; lang_fr: number; topic_fraud: number };
  pseudonmized: boolean;
  simplified: string;
  text: string;
  topics: { [id: number]: number };
  extended: Array<{
    original: string;
    text: string;
    solution: string;
    tokens: Array<{
      text: string;
      topics: { [id: number]: number };
      language: string;
      altSpellings: [];
      altSenses: [];
      leetMappings: [];
      flags: [];
      taskIds: [];
      dateCreated: number;
      dateUpdated: number;
      clientId: number;
    }>;
  }>;
}
