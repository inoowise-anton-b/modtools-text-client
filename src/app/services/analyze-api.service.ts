/*

We would like to integrate state management system like NgRx or MobX.
But we don't think it is okay to inte

*/

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ClassifyTextResponse } from '../models/api/classify-api.models';

@Injectable({
  providedIn: 'root'
})
export class AnalyzeApiService {
  public classifiedText: Subject<ClassifyTextResponse> = new Subject();

  constructor(private http: HttpClient) {}

  public classifyText(
    language: string,
    text: string,
    contentType: string
  ): void {
    this.http
      .post<ClassifyTextResponse>(
        `${environment.apiBaseUrl}/classifyText/2.0.1/classify/text?extended=true`,
        {
          clientId: 60,
          language,
          text,
          contentType
        }
      )
      .subscribe(data => {
        this.classifiedText.next(data);
      });
  }
}
